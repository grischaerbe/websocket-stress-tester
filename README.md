# Websocket Stress Tester

Requirements:
- Node
- Good internet connection

Usage:
- clone repository
```
$ cd /path/to/repository
$ npm i
$ node stresstest-manager spawnIntervalInMilliseconds Amount RunTimeAfterSpawningInSeconds
```
