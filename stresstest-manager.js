const { fork } = require('child_process')
const kill  = require('tree-kill')

if (process.argv.length < 5) {
  console.log('Usage:\n\nnode stresstest-manager spawnIntervalInMilliseconds Amount RunTimeAfterSpawningInSeconds')
  process.exit()
}

const options = {
  interval: parseInt(process.argv[2]),
  amount: parseInt(process.argv[3]),
  runTime: parseInt(process.argv[4]),
}

const processes = []

let currentAmount = 0

let spawnInterval = setInterval(() => {

  const ws = fork('./stresstest.js', [String(currentAmount + 1)])
  processes.push(ws)

  if (currentAmount >= (options.amount - 1)) {
    clearInterval(spawnInterval)

    setTimeout(() => {

      console.log('killing!')
      processes.forEach((child, index) => {
        console.log('CHILD', child.pid)
        kill(child.pid)
      })

    }, options.runTime * 1000)

  }

  currentAmount += 1

}, options.interval)
