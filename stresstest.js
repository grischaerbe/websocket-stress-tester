var WebSocketClient = require('websocket').client

var client = new WebSocketClient()
let receivedData = false

client.on('connectFailed', function(error) {
    console.log('Connection error on Websocket #' + process.argv[2])
})

client.on('connect', function(connection) {
    console.log('WebSocket #' + process.argv[2] + ' connected')
    connection.on('error', function(error) {
        console.log('Error on Websocket #' + process.argv[2])
    })
    connection.on('close', function() {
        console.log('Closing Connection on Websocket #' + process.argv[2])
    })
    connection.on('message', function(message) {
        if (!receivedData) {
            receivedData = true
            console.log('Receiving Data on Websocket #' + process.argv[2])
        }
    })
})

client.connect('wss://livedist.legrisch.com:8080', 'echo-protocol')
